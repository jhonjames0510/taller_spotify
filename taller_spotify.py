from datetime import datetime, timezone
import requests as req
import pandas as pd
import psycopg2

from sqlalchemy import create_engine

CLIENT_ID = '4acbc5f9fd6d4b6e863b2ea45756dda9'
CLIENT_SECRET = '0c3538f5487849f29ab1efffc3703ccd'
AUTH_URL = 'https://accounts.spotify.com/api/token'

# POST
auth_response = req.post(AUTH_URL, {
    'grant_type': 'client_credentials',
    'client_id': CLIENT_ID,
    'client_secret': CLIENT_SECRET,
})

# convert the response to JSON
auth_response_data = auth_response.json()

# save the access token
access_token = auth_response_data['access_token']

headers = {
    'Authorization': 'Bearer {token}'.format(token=access_token)
}

artists_json = []
albums_json = []
albums = []

def readArtist(artist_id):
    # GET request with proper header
    res_artist = req.get(BASE_URL + 'artists/' + artist_id, headers=headers)
    res_artist = res_artist.json()
    res_artist.update({
        'followers': res_artist['followers']['total']
    })
    artists_json.append(res_artist)

    # GET request with proper header

    res_albums = req.get(BASE_URL + 'artists/' + artist_id + '/albums', headers=headers, params={'include_groups': 'album', 'limit': 10})
    duplicate = res_albums.json()

    for album in duplicate['items']:
        album_name = album['name']

        # pull all tracks from this album
        res_albums = req.get(BASE_URL + 'albums/' + album['id'] + '/tracks',
                         headers=headers)
        tracks = res_albums.json()['items']

        for track in tracks:
            # get audio features (key, liveness, danceability, ...)
            res_tracks = req.get(BASE_URL + 'tracks/' + track['id'],
                             headers=headers)
            res_tracks = res_tracks.json()

            res_tracks.update({
                'artists': res_artist['name'],
                'release_date': album['release_date'],
                'album': album_name,
                'genres': res_artist['genres']
            })
            albums_json.append(res_tracks)


# base URL of all Spotify API endpoints
BASE_URL = 'https://api.spotify.com/v1/'

# Artists ID from the URI
artist_ids = ['2ye2Wgw4gimLv2eAKyk1NB', '12Chz98pHFMPJEknJQMWvI', '0L8ExT028jH3ddEcZwqJJ5', '7jy3rLJdDQY21OgRLCZ9sD', '3AA28KZvwAUcZuOKwyblJQ']

for artist in artist_ids:
    readArtist(artist)

dt = datetime.utcnow()

data_df_artist = pd.DataFrame(artists_json)
data_df_artist.loc[:,'upload_date'] = dt.replace(tzinfo=timezone.utc).timestamp()
data_df_artist.loc[:,'origin']= 'spotify_api'
data_to_db_artist = data_df_artist[['followers', 'name', 'popularity', 'type', 'uri', 'origin', 'upload_date']]

data_df_track = pd.DataFrame(albums_json)
data_df_track.loc[:,'origin']= 'spotify_api'
data_df_track.loc[:,'upload_date'] = dt.replace(tzinfo=timezone.utc).timestamp()
data_to_db_track = data_df_track[['name', 'type', 'artists', 'album','track_number', 'popularity', 'id', 'uri', 'release_date', 'genres', 'origin', 'upload_date']]


# Load to DB
engine = create_engine('postgresql://postgres:jhon0223@127.0.0.1:5432/spotify')
data_to_db_artist.to_sql('artist', con=engine, index=False)
data_to_db_track.to_sql('tracks', con=engine, index=False)